package fun.ticsmyc.rpc.server;

/**
 * 在容器关闭时，从nacos取消所有服务的注册
 *
 * 在fun.ticsmyc.rpc.nacos.registry.impl.NacosServiceRegistryImpl使用Runtime.getRuntime().addShutdownHook
 * 注册了一个回调函数，在jvm停止时做同样的事情。
 *
 *
 * @author Ticsmyc
 * @date 2020-11-20 10:19
 */

import fun.ticsmyc.rpc.nacos.registry.ServiceRegistry;
import fun.ticsmyc.rpc.nacos.registry.impl.NacosServiceRegistryImpl;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

@Component
public class ServiceDeregister implements DisposableBean {

    private ServiceRegistry serviceRegistry = NacosServiceRegistryImpl.getInstance();
    @Override
    public void destroy()  {
        serviceRegistry.deregisterAll();
    }
}
