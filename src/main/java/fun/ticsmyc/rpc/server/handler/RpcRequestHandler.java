package fun.ticsmyc.rpc.server.handler;

import fun.ticsmyc.rpc.common.entity.RpcRequest;
import fun.ticsmyc.rpc.common.entity.TRPCServiceProperties;
import fun.ticsmyc.rpc.server.provider.ServiceProvider;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 根据rpcRequest 调用相应实现类， 返回执行结果
 * @author Ticsmyc
 * @date 2020-10-23 22:03
 */
public class RpcRequestHandler {


    public static Object invokeMethod(ServiceProvider serviceProvider, RpcRequest rpcRequest) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TRPCServiceProperties rpcServiceProperties = TRPCServiceProperties.builder()
                .serviceName(rpcRequest.getInterfaceName())
                .group(rpcRequest.getGroup())
                .build();
        Object service = serviceProvider.getService(rpcServiceProperties.getSignature());
        Method method = service.getClass().getMethod(rpcRequest.getMethodName(), rpcRequest.getParamTypes());
        return method.invoke(service, rpcRequest.getParameters());
    }

}
