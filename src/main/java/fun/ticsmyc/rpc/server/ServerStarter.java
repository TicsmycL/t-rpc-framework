package fun.ticsmyc.rpc.server;

import fun.ticsmyc.rpc.Config;
import fun.ticsmyc.rpc.server.transport.bio.BioRpcServer;
import fun.ticsmyc.rpc.server.transport.netty.NettyRpcServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author Ticsmyc
 * @date 2020-11-26 10:27
 */
@Component
public class ServerStarter {
    @Autowired
    private NettyRpcServer nettyRpcServer;

    @Autowired
    private BioRpcServer bioRpcServer;

    public void start() throws ExecutionException, InterruptedException {
        CompletableFuture<Void> started = new CompletableFuture<>();
        switch (Config.getNetworkIO()){
            case "netty":
                new Thread(()->nettyRpcServer.startServer(started)).start();
                break;
            case "socket":
                new Thread(()->bioRpcServer.startServer(started)).start();
                break;
            default:
                new Thread(()->nettyRpcServer.startServer(started)).start();
                break;
        }
        started.get();
    }
}
