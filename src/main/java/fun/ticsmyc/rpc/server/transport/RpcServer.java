package fun.ticsmyc.rpc.server.transport;

import fun.ticsmyc.rpc.common.serializer.SerializerCode;

import java.util.concurrent.CompletableFuture;

/**
 * @author Ticsmyc
 * @date 2020-10-23 18:04
 */
public interface RpcServer {

    /**
     * 启动rpc服务器
     */
    void startServer(CompletableFuture<Void> started);


}
