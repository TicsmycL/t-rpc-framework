package fun.ticsmyc.rpc.server.transport.netty;

import fun.ticsmyc.rpc.Config;
import fun.ticsmyc.rpc.server.transport.RpcServer;
import fun.ticsmyc.rpc.server.transport.netty.codec.RequestDecoder;
import fun.ticsmyc.rpc.server.transport.netty.codec.ResponseEncoder;
import fun.ticsmyc.rpc.server.transport.netty.handler.NettyServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * 基于Netty的服务器端
 * @author Ticsmyc
 * @date 2020-10-23 20:41
 */
@Component
public class NettyRpcServer implements RpcServer {
    private static final Logger logger = LoggerFactory.getLogger(NettyRpcServer.class);

    @Override
    public void startServer(CompletableFuture<Void> started) {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        try{
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup,workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO)) //boss的日志
                    .option(ChannelOption.SO_BACKLOG, 256)
                    .option(ChannelOption.SO_KEEPALIVE, true) //长连接
                    .childOption(ChannelOption.TCP_NODELAY, true) //禁用了Nagle算法，允许小包的发送
                    .childHandler(new ChannelInitializer<SocketChannel>(){
                        @Override
                        protected void initChannel(SocketChannel ch) {
                            ChannelPipeline pipeline = ch.pipeline();
                            // 入站：  字节码 -> 解码器：解码成RpcRequest对象 -> 业务Handler
                            pipeline.addLast(new RequestDecoder());
                            // 出站： RpcResponse -> 编码器：编码成字节码 ->发出去
                            pipeline.addLast(new ResponseEncoder(Config.getSerializer()));
                            //心跳 10s读不到就触发一个心跳事件
                            pipeline.addLast(new IdleStateHandler(10,0,0, TimeUnit.SECONDS));
                            //业务Handler
                            pipeline.addLast(new NettyServerHandler(Config.getServiceProvider()));

                        }
                    });
            ChannelFuture channelFuture = bootstrap.bind(Config.getPort()).sync();
            logger.info("服务端启动成功");
            started.complete(null);
            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally{
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }


}
