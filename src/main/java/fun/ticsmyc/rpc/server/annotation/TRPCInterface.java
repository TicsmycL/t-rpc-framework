package fun.ticsmyc.rpc.server.annotation;

import java.lang.annotation.*;

/**
 * @author Ticsmyc
 * @date 2020-11-23 17:02
 */
@Documented //加载文档里
@Retention(RetentionPolicy.RUNTIME) //保留到运行时
@Target({ElementType.TYPE}) // 用在方法上
@Inherited  //允许子类继承
public @interface TRPCInterface {
}
