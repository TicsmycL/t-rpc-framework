package fun.ticsmyc.rpc.server.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author Ticsmyc
 * @date 2020-11-10 11:24
 */
@Documented //加载文档里
@Retention(RetentionPolicy.RUNTIME) //保留到运行时
@Target({ElementType.TYPE}) // 用在方法上
@Inherited  //允许子类继承
@Component
public @interface TRPCService {

    /**
     * 所在分组
     * @return
     */
    String group() default "";
}
