package fun.ticsmyc.rpc.server.provider;

import fun.ticsmyc.rpc.common.entity.TRPCServiceProperties;

/**
 * 服务注册 接口
 * 用于：  根据rpcRequest中的接口名称 获取到 相应的实现类
 * @author Ticsmyc
 * @date 2020-10-23 16:04
 */

public interface ServiceProvider {
    /**
     * 注册服务
     */
    void register(Object service, TRPCServiceProperties trpcServiceProperties);


    /**
     * 根据接口名称获取接口实现
     */
    Object getService(String interfaceName);
}
