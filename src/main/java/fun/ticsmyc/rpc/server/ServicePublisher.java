package fun.ticsmyc.rpc.server;

import fun.ticsmyc.rpc.Config;
import fun.ticsmyc.rpc.common.entity.TRPCServiceProperties;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * @author Ticsmyc
 * @date 2020-11-12 10:00
 */
class ServicePublisher {
    /**
     *   发布服务：
     *   主要做两件事
     *   1. 将接口实现类注册到本地Map，供Server使用
     *   2. 将接口和提供服务的服务器地址注册到Nacos，供Client使用
     */
    public static void publishService(Object service, Class<?> serviceClass, TRPCServiceProperties trpcServiceProperties) {
        Config.getServiceProvider().register(service,trpcServiceProperties);

        try {
            Config.getServiceRegistry().register(trpcServiceProperties,new InetSocketAddress(InetAddress.getLocalHost(),Config.getPort()));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
