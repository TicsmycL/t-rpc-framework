package fun.ticsmyc.rpc.common.enumeration;

/**
 * Rpc调用失败的类型
 * @author Ticsmyc
 * @date 2020-10-23 11:36
 */
public enum RpcError {
    SERVICE_SCAN_PACKAGE_NOT_FOUND("启动类ServiceScan注解缺失"),
    CONNECT_ERROR("连接服务端失败"),
    SERVICE_INVOCATION_FAILURE("服务调用失败"),
    SERVICE_NOT_FOUNT("找不到对应的服务"),
    NO_AVAILABLE_SERVICE("找不到可用服务"),
    SERVICE_NOT_IMPLEMENT_ANY_INTERFACE("注册的服务未实现接口"),
    UNKNOW_PROTOCOL("不识别的协议包"),
    UNKNOWN_SERIALIZER("不识别的(反)序列化器"),
    CLIENT_CONNECT_SERVER_FAILURE("客户端连接服务端失败"),
    FAILED_TO_CONNECT_TO_SERVICE_REGISTRY("连接注册中心失败"),
    REGISTER_SERVICE_FAILED("服务注册失败"),
    DEREGISTER_SERVICE_FAILED("删除服务失败"),
    RESPONSE_NOT_MATCH("响应与请求号不匹配");


    private final String message;

    RpcError(String mes){
        this.message = mes;
    }

    public String getMessage() {
        return message;
    }
}
