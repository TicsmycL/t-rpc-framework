package fun.ticsmyc.rpc.common.enumeration;

/**
 * RPC响应中用到的失败类型
 * @author Ticsmyc
 * @date 2020-10-23 10:57
 */

public enum ResponseCode {

    SUCCESS(200,"调用成功"),
    FAIL(500,"调用失败"),
    METHOD_NOT_FOUND(404,"未找到指定方法"),
    CLASS_NOT_FOUND(405,"未找到指定类");

    private final int code;
    private final String message;

    ResponseCode(int code, String message){
        this.code = code;
        this.message=message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
