package fun.ticsmyc.rpc.common.enumeration;

/**
 * @author Ticsmyc
 * @date 2020-11-02 16:21
 */
public enum InitializeError {
    RPC_REQUEST_SENDER_FACTORY_NOT_INIT("RpcRequestSenderFactory没有初始化");

    private final String msg;
    InitializeError(String msg){
        this.msg = msg;
    }
    public String getMsg(){
        return msg;
    }
}
