package fun.ticsmyc.rpc.common.serializer;

/**
 * @author Ticsmyc
 * @date 2020-10-28 11:38
 */
public enum SerializerCode {
    KRYO(0),
    JSON(1),
    HESSIAN(2),
    PROTOBUF(3);

    private final byte code;

    SerializerCode(int code){
        this.code =(byte)code;
    }

    public byte getCode() {
        return code;
    }
}
