package fun.ticsmyc.rpc.common.serializer;

import fun.ticsmyc.rpc.common.serializer.impl.HessianSerializer;
import fun.ticsmyc.rpc.common.serializer.impl.JsonSerializer;
import fun.ticsmyc.rpc.common.serializer.impl.KryoSerializer;
import fun.ticsmyc.rpc.common.serializer.impl.ProtobufSerializer;

/**
 * @author Ticsmyc
 * @date 2020-10-28 11:06
 */
public class Serializers {

    public static Serializer getSerializerByCode(byte code){
        switch (code) {
            case 0:
                return KryoSerializer.getInstance();
            case 1:
                return JsonSerializer.getInstance();
            case 2:
                return HessianSerializer.getInstance();
            case 3:
                return ProtobufSerializer.getInstance();
            default:
                return KryoSerializer.getInstance();
        }
    }

    public static Serializer getSerializerByName(String serializer){
        switch (serializer) {
            case "kryo":
                return KryoSerializer.getInstance();
            case "json":
                return JsonSerializer.getInstance();
            case "hessian":
                return HessianSerializer.getInstance();
            case "protobuf":
                return ProtobufSerializer.getInstance();
            default:
                return KryoSerializer.getInstance();
        }
    }


}
