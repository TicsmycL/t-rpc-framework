package fun.ticsmyc.rpc.common.serializer;

/**
 * 序列化器通用接口
 * @author Ticsmyc
 * @date 2020-10-26 11:01
 */

public interface Serializer {

    byte[] serialize(Object obj);

    Object deserialize(byte[] bytes, Class<?> clazz);

    byte getId();

}
