package fun.ticsmyc.rpc.common.entity;

import lombok.*;

/**
 * 用于记录 提供Rpc调用的方法的相关信息
 * 包括服务名（接口的name）、服务分组
 * @author Ticsmyc
 * @date 2020-11-17 9:57
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TRPCServiceProperties {

    private String group;

    private String serviceName;

    /**
     * 获取方法签名（在注册中心注册的名字）
     * @return
     */
    public String getSignature() {
        return this.getServiceName()+"_"+this.getGroup();
    }

    @Override
    public String toString() {
        return "TRPCServiceProperties{" +
                "group='" + group + '\'' +
                ", serviceName='" + serviceName + '\'' +
                '}';
    }
}
