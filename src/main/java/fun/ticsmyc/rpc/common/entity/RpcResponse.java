package fun.ticsmyc.rpc.common.entity;

import fun.ticsmyc.rpc.common.enumeration.ResponseCode;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Ticsmyc
 * @date 2020-10-23 10:54
 */
@Data
public class RpcResponse<T> implements Serializable {

    /**
     * 请求对应的相应号
     */
    private String requestId;
    /**
     * 响应状态码
     */
    private Integer statusCode;

    /**
     * 响应状态补充信息
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;


    public static <T> RpcResponse<T> success(T data,String requestId){
        RpcResponse<T> response = new RpcResponse<>();
        response.setRequestId(requestId);
        response.setStatusCode(ResponseCode.SUCCESS.getCode());
        response.setMessage(ResponseCode.SUCCESS.getMessage());
        response.setData(data);
        return response;
    }

    public static <T> RpcResponse<T> fail(ResponseCode code,String requestId){
        RpcResponse<T> response = new RpcResponse<>();
        response.setRequestId(requestId);
        response.setStatusCode(code.getCode());
        response.setMessage(code.getMessage());
        return response;
    }

}
