package fun.ticsmyc.rpc.common.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 消费者向生产者发送的请求对象
 * @author Ticsmyc
 * @date 2020-10-23 9:58
 */
@Data
public class RpcRequest implements Serializable {

    /**
     * 请求号
     */
    private String requestId;
    /**
     * 待调用接口名称
     */
    private String interfaceName;

    /**
     * 待调用方法名称
     */
    private String methodName;

    /**
     * 调用方法的参数类型
     */
    private Class<?>[] paramTypes;
    /**
     * 方法参数
     */
    private Object[] parameters;

    /**
     * 是否是心跳包
     */
    private Boolean heartBeat;

    private String group;

    @Override
    public String toString() {
        return "RpcRequest{" +
                "requestId='" + requestId + '\'' +
                ", interfaceName='" + interfaceName + '\'' +
                ", methodName='" + methodName + '\'' +
                ", paramTypes=" + Arrays.toString(paramTypes) +
                ", parameters=" + Arrays.toString(parameters) +
                ", heartBeat=" + heartBeat +
                ", group='" + group + '\'' +
                '}';
    }
}
