package fun.ticsmyc.rpc.common.exception;

import fun.ticsmyc.rpc.common.enumeration.RpcError;

/**
 * @author Ticsmyc
 * @date 2020-10-23 11:40
 */
public class RpcException extends RuntimeException {

    public RpcException(RpcError error,String detail){
        super(error.getMessage()+" : "+detail);
    }

    public RpcException(String message,Throwable cause){
        super(message,cause);
    }
    public RpcException(RpcError error){
        super(error.getMessage());
    }
}
