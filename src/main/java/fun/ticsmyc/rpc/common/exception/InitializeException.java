package fun.ticsmyc.rpc.common.exception;

import fun.ticsmyc.rpc.common.enumeration.InitializeError;

/**
 * 系统初始化异常
 * @author Ticsmyc
 * @date 2020-11-02 16:19
 */
public class InitializeException extends RuntimeException {
    public InitializeException(InitializeError error){
        super(error.getMsg());
    }
    public InitializeException(String msg,Throwable cause){
        super(msg,cause);
    }
    public InitializeException(InitializeError error, String detail){
        super(error.getMsg()+" : "+detail);
    }
}
