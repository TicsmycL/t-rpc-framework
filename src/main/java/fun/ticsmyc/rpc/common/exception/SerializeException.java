package fun.ticsmyc.rpc.common.exception;

/**
 * @author Ticsmyc
 * @date 2020-10-26 11:05
 */
public class SerializeException extends RuntimeException{

    public SerializeException(String msg){
        super(msg);
    }
}
