package fun.ticsmyc.rpc.nacos.loadbalance;

/**
 * @author Ticsmyc
 * @date 2020-10-30 12:07
 */
public enum LoadBalancerEnum {
    RANDOM(0),
    ROUND_ROBIN(1);
    private final byte code;

    LoadBalancerEnum(int code){
        this.code =(byte)code;
    }

    public byte getCode() {
        return code;
    }

}
