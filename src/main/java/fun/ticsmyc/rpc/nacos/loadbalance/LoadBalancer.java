package fun.ticsmyc.rpc.nacos.loadbalance;

import com.alibaba.nacos.api.naming.pojo.Instance;

import java.util.List;

/**
 * @author Ticsmyc
 * @date 2020-10-30 11:44
 */

public interface LoadBalancer {
    Instance select(List<Instance> instances);
}
