package fun.ticsmyc.rpc.nacos.loadbalance;

import fun.ticsmyc.rpc.nacos.loadbalance.impl.RandomNacosLoadBalancer;
import fun.ticsmyc.rpc.nacos.loadbalance.impl.RoundRobinNacosLoadBalancer;

/**
 * @author Ticsmyc
 * @date 2020-10-30 12:08
 */
public class NacosLoadBalancers {

    public static LoadBalancer getLoadBalancer(LoadBalancerEnum loadBalancerEnum){
        switch (loadBalancerEnum) {
            case RANDOM:
                return RandomNacosLoadBalancer.getInstance();
            case ROUND_ROBIN:
                return RoundRobinNacosLoadBalancer.getInstance();
            default:
                return RandomNacosLoadBalancer.getInstance();
        }
    }

    public static LoadBalancer getLoadBalancer(String loadBalancer){
        switch (loadBalancer) {
            case "default":
                return RandomNacosLoadBalancer.getInstance();
            case "round":
                return RoundRobinNacosLoadBalancer.getInstance();
            case "random":
                return RandomNacosLoadBalancer.getInstance();
            default:
                return RandomNacosLoadBalancer.getInstance();
        }
    }
}
