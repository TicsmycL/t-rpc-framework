package fun.ticsmyc.rpc.nacos.registry;


import java.net.InetSocketAddress;

/**
 * @author Ticsmyc
 * @date 2020-10-30 11:51
 */

public interface ServiceDiscovery {
    InetSocketAddress lookupService(String serviceName);

    InetSocketAddress lookupService(String serviceName,int retryTime);
}
