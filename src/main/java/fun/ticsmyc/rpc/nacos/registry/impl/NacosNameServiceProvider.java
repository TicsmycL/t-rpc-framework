package fun.ticsmyc.rpc.nacos.registry.impl;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import fun.ticsmyc.rpc.Config;
import fun.ticsmyc.rpc.common.enumeration.RpcError;
import fun.ticsmyc.rpc.common.exception.RpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 在服务启动时就创建与注册中心的连接
 * @author Ticsmyc
 * @date 2020-10-30 11:54
 */
public class NacosNameServiceProvider {
    private static final Logger logger = LoggerFactory.getLogger(NacosNameServiceProvider.class);

    private static final NamingService namingService;

    static{
        try{
            namingService = NamingFactory.createNamingService(Config.getConfigCenterAddress());
        }catch(NacosException e){
            logger.error("连接到Nacos时有错误发生");
            throw new RpcException(RpcError.FAILED_TO_CONNECT_TO_SERVICE_REGISTRY);
        }
    }

    public static NamingService getInstance(){
        return namingService;
    }

    private NacosNameServiceProvider(){}
}
