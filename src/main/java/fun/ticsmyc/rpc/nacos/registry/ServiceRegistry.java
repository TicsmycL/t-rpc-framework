package fun.ticsmyc.rpc.nacos.registry;

import fun.ticsmyc.rpc.common.entity.TRPCServiceProperties;

import java.net.InetSocketAddress;

/**
 * @author Ticsmyc
 * @date 2020-10-29 11:11
 */

public interface ServiceRegistry {
    void register(TRPCServiceProperties trpcServiceProperties, InetSocketAddress inetSocketAddress);

    void deregister(TRPCServiceProperties trpcServiceProperties, InetSocketAddress inetSocketAddress);

    void deregisterAll();
}
