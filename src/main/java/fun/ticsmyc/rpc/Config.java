package fun.ticsmyc.rpc;

import fun.ticsmyc.rpc.client.transport.RpcClient;
import fun.ticsmyc.rpc.client.transport.bio.BioRpcClient;
import fun.ticsmyc.rpc.client.transport.netty.NettyRpcClient;
import fun.ticsmyc.rpc.common.serializer.Serializer;
import fun.ticsmyc.rpc.common.serializer.SerializerCode;
import fun.ticsmyc.rpc.common.serializer.Serializers;
import fun.ticsmyc.rpc.nacos.loadbalance.LoadBalancer;
import fun.ticsmyc.rpc.nacos.loadbalance.LoadBalancerEnum;
import fun.ticsmyc.rpc.nacos.loadbalance.NacosLoadBalancers;
import fun.ticsmyc.rpc.nacos.registry.ServiceRegistry;
import fun.ticsmyc.rpc.nacos.registry.impl.NacosServiceRegistryImpl;
import fun.ticsmyc.rpc.server.provider.ServiceProvider;
import fun.ticsmyc.rpc.server.provider.impl.DefaultServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Ticsmyc
 * @date 2020-11-12 9:50
 */
public class Config {
    private static final Logger logger = LoggerFactory.getLogger(Config.class);
    private static final String CONFIG_FILE = "/trpc.properties";

    /**
     * 一些默认配置
     */
    private final static SerializerCode DEFAULT_SERIALIZER = SerializerCode.KRYO;
    private final static LoadBalancerEnum DEAFULT_LOAD_BALANCER = LoadBalancerEnum.RANDOM;
    private final static Integer DEFAULT_PORT = 8888;
    private final static String DEFAULT_CONFIG= "127.0.0.1:8848";


    static{
        Properties prop = new Properties();
        try {

            InputStream path = Config.class.getClassLoader().getResourceAsStream(CONFIG_FILE);
            prop.load(path);
        } catch (Exception e) {
            try{
                InputStream path = Config.class.getResourceAsStream(CONFIG_FILE);
                prop.load(path);
            } catch (IOException ioException) {
                logger.info("未找到配置文件，使用默认配置");
            }
        }

        port = prop.getProperty("port") == null ? DEFAULT_PORT: Integer.valueOf(prop.getProperty("port"));
        loadBalancer = prop.getProperty("loadbalancer") ==null ?
                NacosLoadBalancers.getLoadBalancer(DEAFULT_LOAD_BALANCER):NacosLoadBalancers.getLoadBalancer(prop.getProperty("loadbalancer"));
        serializer =prop.getProperty("serializer") ==null ?
                Serializers.getSerializerByCode(DEFAULT_SERIALIZER.getCode())
                :Serializers.getSerializerByName(prop.getProperty("serializer"));
        networkIO =prop.getProperty("networkIO") ==null ? "netty" :prop.getProperty("networkIO");
        configCenterAddress = prop.getProperty("nameServiceAddress")==null ? DEFAULT_CONFIG : prop.getProperty("nameServiceAddress");
    }

    /**
     * 服务本地注册和提供，现在只用ConcurrentHashMap实现
     */
    private static final ServiceProvider serviceProvider = DefaultServiceProvider.getInstance();
    /**
     * 服务的注册中心， 目前只有nacos一种
     */
    private static final ServiceRegistry serviceRegistry = NacosServiceRegistryImpl.getInstance();

    /**
     * 序列化器，目前有4种
     */
    private static final Serializer serializer ;
     /**
     * 消费服务时使用的负载均衡器，目前有两种
     */
    private static final LoadBalancer loadBalancer;
    /**
     * 提供服务时使用的端口
     */
    private static final Integer port;

    private static final String networkIO;

    /**
     * 配置中心地址
     */
    private static final String configCenterAddress;



    public static String getNetworkIO() {
        return networkIO;
    }

    public static RpcClient getRpcClient(){
        switch (networkIO){
            case "netty":
                return NettyRpcClient.getInstance();
            case "socket":
                return BioRpcClient.getInstance();
            default:
                return NettyRpcClient.getInstance();
        }
    }

    public static Serializer getSerializer() {
        return serializer;
    }

    public static ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public static Integer getPort() {
        return port;
    }

    public static ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public static LoadBalancer getLoadBalancer() {
        return loadBalancer;
    }

    public static String getConfigCenterAddress() {
        return configCenterAddress;
    }
}
