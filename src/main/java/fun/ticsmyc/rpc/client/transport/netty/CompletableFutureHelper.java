package fun.ticsmyc.rpc.client.transport.netty;

import fun.ticsmyc.rpc.common.entity.RpcResponse;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用于异步获取RpcResponse
 * @author Ticsmyc
 * @date 2020-11-02 11:33
 */
public class CompletableFutureHelper {
    private static Map<String, CompletableFuture<RpcResponse>> map = new ConcurrentHashMap<>();

    public void put(String requestId, CompletableFuture<RpcResponse> future){
        map.put(requestId,future);
    }

    public void remove(String requestId){
        map.remove(requestId);
    }

    public void complete(RpcResponse rpcResponse){
        CompletableFuture<RpcResponse> future = map.get(rpcResponse.getRequestId());
        //事件结束， 返回值设置为rpcResponse
        future.complete(rpcResponse);
        map.remove(rpcResponse.getRequestId());
    }


}
