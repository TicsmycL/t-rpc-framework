package fun.ticsmyc.rpc.client.transport;

import fun.ticsmyc.rpc.common.entity.RpcRequest;
import fun.ticsmyc.rpc.common.entity.TRPCServiceProperties;
import fun.ticsmyc.rpc.common.serializer.SerializerCode;
import fun.ticsmyc.rpc.nacos.loadbalance.LoadBalancerEnum;

/**
 * @author Ticsmyc
 * @date 2020-10-23 18:06
 */
public interface RpcClient {

    //最大重试次数
    int MAX_RETRY_COUNT =5;

    Object sendRequest(RpcRequest rpcRequest,TRPCServiceProperties trpcServiceProperties);
}
