package fun.ticsmyc.rpc.client.annotation;

import java.lang.annotation.*;

/**
 * @author Ticsmyc
 * @date 2020-11-18 21:53
 */
@Documented //加载文档里
@Retention(RetentionPolicy.RUNTIME) //保留到运行时
@Target({ElementType.FIELD}) // 用在属性上
@Inherited  //允许子类继承
public @interface RpcClient {

    /**
     * 指定组名
     * @return
     */
    String group() default "";
}
