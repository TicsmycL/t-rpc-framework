package fun.ticsmyc.rpc;

import fun.ticsmyc.rpc.server.ServerStarter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutionException;

/**
 * @author Ticsmyc
 * @date 2020-11-23 16:18
 */
@Configuration
@ComponentScan("fun.ticsmyc.rpc")
public class TrpcStarter implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger logger = LoggerFactory.getLogger(TrpcStarter.class);

    @Autowired
    private ServerStarter serverStarter;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if(event.getApplicationContext().getParent() == null){
            try {
                serverStarter.start();
                logger.info("TRPC启动完毕");
            } catch (Exception e) {
                logger.error("TRPC启动失败");
            }

        }
    }
}
